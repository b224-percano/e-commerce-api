const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        productName: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            default: 1
        },
        price: {
            type: Number,
            required: true
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    contactNo: {
        type: String,
        required: true
    },
    isPaid: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        default: "pending"
    }

},
{timestamps: true}
);

module.exports = mongoose.model("Order", orderSchema);

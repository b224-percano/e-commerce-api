const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator")

const cartSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            require: true,
            unique: true
        },
        productName: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            default: 1
        },
        price: {
            type: Number,
            required: true
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    }
},
{timestamps: true}
);

cartSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Cart", cartSchema);

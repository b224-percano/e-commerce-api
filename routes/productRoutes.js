const express = require("express");
const { verify } = require("jsonwebtoken");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");


// Routes for creating a product(ADMIN/SELLER)
router.post("/addProduct", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin === false){
        res.status(403).send("Unauthorized to view this")
    }else{
        productController.addProduct(userData, req.body).then((resultFromController) => {
            res.send(resultFromController)
        })
    }
});


// Routes for retriving product
router.get("/", (req, res) => {
	productController.getActiveProducts().then((resultFromController) => res.send(resultFromController))
});

// Routes for retieving a single product
router.get("/find/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController))
});


// Routes for updating a single product(ADMIN/SELLER)
router.patch("/update/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.updateProduct(userData, req.params,req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});

// Routes for archiving a single product
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.archiveProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});


// Routes for activating a single product
router.patch("/activate/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.activateProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("You're not an ADMIN")
	}
});


// Routes for retrieving all product(ADMIN SELLER)
router.get("/allProducts", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
        res.status(403).send("Unauthorized to view this")
	}else{
		productController.allProduct(userData).then((resultFromController) => res.send(resultFromController))
	}
});


// Routes for retriving products by category
router.get("/allProductsByCategory", (req, res) =>{
	productController.productByCategory(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
});

// Rooutes for retriving 3 hot products
router.get("/hotProducts", (req, res) => {
	productController.hotProduct().then((resultFromController) => {
		res.send(resultFromController)
	})
});








module.exports = router;

const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

// Routes for  user registration
router.post("/register", (req, res) => {
    userController.userRegistration(req.body).then((resultFromController) => {
        res.send(resultFromController)
    })
});


// Routes for user login
router.post("/login", (req, res) => {
    userController.userLogin(req.body).then((resultFromController) => {
        res.send(resultFromController)
    })
});

// Routes for getting user details
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getDetails(userData).then((resultFromController => {
        res.send(resultFromController)
    }))
});

// Routes for setting user as Admin (OWNER ONLY)
router.patch("/setAdmin", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin === true){
        userController.setAdmin(req.body).then((resultFromController) => res.send(resultFromController))
    }else {
        res.status(403).send("You're not the owner")
    }
});


// Routes for getting all user details (Owner ONLY)
router.get("/getAllDetails", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin === true){
        userController.getAllDetails().then((resultFromController) => res.send(resultFromController))
    }else {
        res.status(403).send("You're not the owner")
    }
});






module.exports = router;

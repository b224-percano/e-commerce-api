// EXPRESS SERVER SETUP
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");


const app = express();
const port = process.env.PORT || 10000;


// Mongoose Connection Setup
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://aljhnprcno:admin123@batch224-percano.l8zics7.mongodb.net/Ecommerce-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
.then(() => console.log("Connected to MongoDB"))
.catch((error) => console.log("Connection Error"));


// Middlewares
// Allows our frontend app to access our backend app.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);








app.listen(port, () => {console.log(`API is now running at port: ${port}`)});
const Cart = require("../models/Cart");
const Product = require("../models/Product");
const Order = require("../models/Order");


// Cart Controller Function

// Adding Products to Cart
module.exports.addToCart = async (userData, reqParams, reqBody) => {
    try{
        const userCarts = await Cart.find({userId: userData.id})
        const orderedProduct = await Product.findById(reqParams.productId)
        let cart = new Cart ({
            userId: userData.id,
            products: [{
                productId: orderedProduct.id,
                productName: orderedProduct.name,
                quantity: reqBody.quantity || 1,
                price: orderedProduct.price
            }],
            totalAmount: orderedProduct.price * (reqBody.quantity || 1)
        })

        if(orderedProduct === null || orderedProduct.stock - reqBody.quantity < 0){
           return false 
        }else if(userCarts.length === 0){
            await cart.save()
            return true
        }else{
            let newCart = []
            let amount = 0
            userCarts.forEach((userCart) => {
                let isDupe = false
                userCart.products.forEach(product => {
                    if(product.productId === orderedProduct.id){
                        isDupe = true
                    }
                })
                if(orderedProduct.shopName === userCart.shopName && isDupe === false ){
                    newCart.push(userCart)
                }
            })
            if(newCart.length > 0){
                newCart[0].products.push({
                    productId: orderedProduct.id,
                    productName: orderedProduct.name,
                    quantity: reqBody.quantity || 1,
                    price: orderedProduct.price
                })
                newCart[0].products.forEach(product => {
                    amount += (product.price * reqBody.quantity)
                })
                newCart[0].totalAmount = amount
                await Cart.findByIdAndUpdate(newCart[0].id, newCart[0])
                return true
            }else {
                await cart.save()
                return true
            }
        }
    }catch(error){
        return (error._message ? "Duplicate product on your cart" : false)
    }
};


// GET User Cart
module.exports.getCart = async (userData) => {
    try{
        const userCart = await Cart.find({userId: userData.id})
        return (userCart.length === 0 ? false : userCart)
    }catch(error){
        return error
    }
};


// Updating Qty of Products in cart
module.exports.updateCart =  async (userData, reqParams, reqBody) => {
    try {
        const userCart = await Cart.findById(reqParams.cartId)
        if(userCart === null || userCart.userId !== userData.id){
            return false
        }else {
            userCart.totalAmount = 0;
            userCart.products.forEach((product => {
                if(product.productId === reqBody.productId){
                    product.quantity = reqBody.quantity || 1
                }
                userCart.totalAmount += product.price * product.quantity
            }))
            await Cart.findByIdAndUpdate(userCart.id, userCart)
            return true
        }
    }catch(error){
        return error
    }
};


// Deleting Products in cart
module.exports.deleteCart = async (userData, reqParams ,reqBody) => {
    try {
        const userCart =  await Cart.findById(reqParams.cartId)
        if(userCart ===  null || userCart.userId !== userData.id){
            return false
        }else{
            userCart.totalAmount = 0;
            userCart.products.forEach((product) => {
                if(product.productId === reqBody.productId){
                    const indexOfProdToRemove = userCart.products.indexOf(product)
                    userCart.products.splice(indexOfProdToRemove, 1)
                }
                userCart.totalAmount += product.price * product.quantity
            })
            if(userCart.products.length === 0){
                await Cart.findByIdAndDelete(reqParams.cartId)
                return true
            }else{
                await Cart.findByIdAndUpdate(userCart.id, userCart)
                return true
            }
        }
    }catch(error){
        return error
    }
};


// Checkout Cart
module.exports.orderCart = async (userData, reqBody) => {
    try{
        const userCarts = await Cart.find({userId: userData.id})
        if(userCarts.length ===0){
            return false
        }else {
            for(i = 0; i < userCarts.length; i++){
                let order = new Order ({
                    userId: userData.id,
                    userName: userData.name,
                    products: [],
                    totalAmount: userCarts[i].totalAmount,
                    address: reqBody.address,
                    contactNo: reqBody.contactNo
                })

                userCarts[i].products.forEach( async (product) => {
                    order.products.push({
                        shopName: userCarts[i].shopName,
                        productId: product.productId,
                        productName: product.productName,
                        quantity: product.quantity,
                        price: product.price
                    })

                    let cartProduct = await Product.findById(product.productId)
                    let update = {
                        stock: cartProduct.stock - product.quantity,
                        sold: cartProduct.sold + product.quantity
                    }
                    if(update.stock <= 0){
                        update ={
                            ...update,
                            isActive: false
                        }
                    }
                    await Product.findByIdAndUpdate(cartProduct.id, update)
                })
                await order.save()
                await Cart.findByIdAndDelete(userCarts[i].id)
            }
            return true
        }
    }catch(error){
        return error
    }
}

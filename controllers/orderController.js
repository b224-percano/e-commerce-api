const Order = require("../models/Order");
const Product = require("../models/Product");


// Function for create order (BUYER ONLY)
module.exports.createOrder = async (userData, reqParams, reqBody) => {
    try{
        const orderedProduct = await Product.findById(reqParams.productId)
        if(orderedProduct === null){
            return false
        }else if (orderedProduct.stock - reqBody.quantity < 0){
            return false
        }else {
            let order = new Order ({
                userId: userData.id,
                userName: userData.name,
                products: [{
                    productId: orderedProduct.id,
                    productName: orderedProduct.name,
                    quantity: reqBody.quantity || 1,
                    price: orderedProduct.price 
                }],
                totalAmount: orderedProduct.price * (reqBody.quantity || 1),
                address: reqBody.address,
                contactNo: reqBody.contactNo
            })
            let update = {
                stock: orderedProduct.stock - (reqBody.quantity || 1),
                sold: orderedProduct.sold + (reqBody.quantity || 1)
            }
            if(update.stock <= 0){
                update = {
                    ...update,
                    isActive: false
                }
            }
            await Product.findByIdAndUpdate(orderedProduct.id, update)
            await order.save()
            return true
        }
    }catch(error){
        return error
    }
};


// Routes for retrieving User Order
module.exports.getOrders = async (userData) => {
    try{
        const res = await Order.find({userId: userData.id})
        console.log(res)
        return (res.length === 0 ? false : res)
    }catch(error){
        return error
    }
};


// Function for updating User Order address/contactNo or cancel if the status is pending (User Only)
// module.exports.updateUserOrder = async (userData, reqParams, reqBody) => {
//     try{
//         const userOrder = await Order.findById(reqParams.orderId)
//         if(userOrder === null || userData.id !== userOrder.userId){
//             return false
//         }else if(userOrder.status === "pending" && userOrder.products.length === 1){
//             let update = {
//                 ...reqBody
//             }
//             if(reqBody.status === "cancelled"){
//                 const orderedProduct = await Product.findById(userOrder.products[0].productId)
//                 let stock = {
//                     stock: orderedProduct.stock + userOrder.products[0].quantity,
//                     sold: orderedProduct.sold - userOrder.products[0].quantity
//                 }
//                 if(userOrder.isActive === false){
//                     stock = {
//                         ...stock,
//                         isActive: true
//                     }
//                 }
//                 await Product.findByIdAndUpdate(orderedProduct.id, stock)
//                 await Order.findByIdAndUpdate(userOrder.id, update)
//                 return true
//             }else{
//                 await Order.findByIdAndUpdate(userOrder.id, update)
//                 return true
//             }
//         }else{
//             return false
//         }
//     }catch(error){
//         return error
//     }
// };


// Function for retrieving all Orders based on status (ADMIN/SELLER)
module.exports.allOrders = async (userData, reqBody) => {
    try {
       const allOrders = await Order.find({status: reqBody.status})
       const orders = []
       allOrders.forEach((order) => {
        if(order.products[0].shopName === userData.shopName){
            orders.push(order)
        }
       })
       return (orders.length === 0 ? false : orders)
    }catch(error){
        return error
    }
};


// Function for updating status of specific Orders (ADMIN/SELLER)
module.exports.updateOrder = async (userData, reqParams, reqBody) => {
    try {
        const orderedProduct  =  await Order.findById(reqParams.orderId)
        let update = {
            status: reqBody.status
        }
        if(orderedProduct === null || userData.shopName !== orderedProduct.products[0].shopName){
            return false
        }else {
            await Order.findByIdAndUpdate(orderedProduct.id, update)
            return true
        }
    }catch(error){
        return error
    }
};

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { update } = require("../models/User");


// Function for user registration
module.exports.userRegistration = async (reqBody) => {
    try {
        let newUser = new User({
            ...reqBody,
            password: bcrypt.hashSync(reqBody.password, 10)
        });
        const res = await newUser.save()
        return (res ? true : false)
    }catch(error){
        return (error.errors.email ? "Email is already taken" : false)
    }
};


// Function for user login
module.exports.userLogin = async (reqBody) => {
    try {
        const res =  await User.findOne({email: reqBody.email})
        if(res === null){
            return false
        }else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, res.password)
            return (isPasswordCorrect ? {access: auth.createAccessToken(res)} : false)
        }
    }catch(error){
        return false
    }
};


// Function for getting details
module.exports.getDetails = async (userData) => {
    try {
        const details =  await User.findById(userData.id)
        if(details){
            details.password = "**********"
            return details
        }else{
            return false
        }
    }catch(error){
        return error
    }
};


// Function for setting user role (OWNER ONLY)
module.exports.setAdmin = async (reqBody) => {
    try{
        const data = await User.findOne({email: reqBody.email})
        if(data == null){
            return false
        }else{
            let update = {
                isAdmin: true
            }
            const makeAdmin = await User.findByIdAndUpdate(data.id, update)
            return true
        }
    }catch(error){
        return error
    }
};


// Function for getting all user details (OWNER ONLY)
module.exports.getAllDetails = async () => {
    try {
        const res = await User.find()
        return (res.length === 0 ? false : res)
    }catch(error){
        return false
    }
};




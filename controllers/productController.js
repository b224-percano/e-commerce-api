const Product = require("../models/Product");


// Function for adding products(ADMIN/SELLER)
module.exports.addProduct = async (userData, reqBody) => {
    try {
        let newProduct = new Product({
            ...reqBody,
            shopName: userData.shopName
        })
        await newProduct.save()
        return true
    }catch(error){
        return (error.errors.name ? "Product Name is already taken" : false)
    }
};


// Function for retrieving products
module.exports.getActiveProducts = async () => {
    try {
        const activeProduct = await Product.find({isActive: true})
        return (activeProduct.length !== 0 ? activeProduct : false)
    }catch(error){
        return error//false
    }
};


// Function for retrieving a single product
module.exports.getProduct = async (reqParams) => {
    try {
        const singleProduct =  await Product.findById(reqParams.productId)
        return (singleProduct && singleProduct.isActive === true ? singleProduct : false)
    }catch(error){
        return error
    }
};


// Function for updating a single product(ADMIN/SELLER)
module.exports.updateProduct = async (userData, reqParams, reqBody)  => {
    try {
        const product = await Product.findById(reqParams.productId)
        let update = {
            ...reqBody
        }
        if(userData.isAdmin === false){
            return false
        }else {
            await Product.findByIdAndUpdate(product.id, update)
            return true
        }
    }catch(error){
        return error
    }
};

// Function for arching a product
module.exports.archiveProduct = async (reqParams) => {
    try {
        let data = {
            isActive: false
        }
        const update = await Product.findByIdAndUpdate(reqParams.productId, data)
        if(update){
            return true
        }else{
            return false
        }
    }catch(error){
        return false
    }

};


// Function for activating a product
module.exports.activateProduct = async (reqParams) => {
    try {
        let data = {
            isActive: true
        }
        const update = await Product.findByIdAndUpdate(reqParams.productId, data)
        if(update){
            return true
        }else{
            return false
        }
    }catch(error){
        return false
    }

};



// Function for retrieving all product(ADMIN SELLER)
module.exports.allProduct = async (userData) => {
    try{
        const products = await Product.find({shopName: userData.shopName})
        return (products ? products : false)
    }catch(error){
        return error
    }
};


// Function for retriving products by category
module.exports.productByCategory = async (reqBody) => {
    try{
        const res = await Product.find({category: reqBody.category, isActive: true})
        return(res.length === 0 ? false : res)
    }catch(error){
        return error
    }
};


// Function for retriving  3 hot products
module.exports.hotProduct = async () => {
    try{
        const hotProducts =  await Product.find({isActive: true})
        .sort({sold: -1})
        .limit(3)
        return (hotProducts.length === 0 ? false : hotProducts)
    }catch(error){
        return error
    }
}






const jwt = require("jsonwebtoken");
const secret = "SecretEcommerceAPI";

// JWT Token
module.exports.createAccessToken = (user) => {
    try {
        const data = {
            id: user._id,
		    name: user.name,
		    email: user.email,
		    isAdmin: user.isAdmin
        };
        return jwt.sign(data,secret, {})
    }catch(error){
        return false
    }
};

// For Authentication
module.exports.verify = (req, res, next) => {
    try {
        let token = req.headers.authorization

        if(typeof token !== "undefined") {
            token = token.slice(7, token.length)
            return jwt.verify(token, secret, (err, data) => {
                return (err ? res.send({auth: "failed"}) : next())
            })
        }else {
            return res.send({auth: failed})
        }
    }catch(error){
        return false
    }
}


// For Authorization
module.exports.decode = (token) => {
    try{
        if(typeof token !== "undefined"){
            token = token.slice(7, token.length)
            return jwt.verify(token, secret, (err, data) => {
                return (err ? null : jwt.decode(token, {complete: true}.payload))
            })
        }else{
            return null
        }
    }catch(error){
        return false
    }
}